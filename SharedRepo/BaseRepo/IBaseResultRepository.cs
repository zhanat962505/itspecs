﻿using SharedRepo.ReposResultDto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SharedRepo.BaseRepo
{
	public interface IBaseResultRepository<TDto>
	{
		Task<EntityListResultDto<TDto>> GetItems(TDto filter);
		Task<OperationResultDto> Add(TDto entity);
		Task<OperationResultDto> Update(TDto entity);
		Task<EntityResultDto<TDto>> GetById(long id);
	}
}
