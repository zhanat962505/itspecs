using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SharedRepo.Repos
{
    public interface IEFBaseRepo<TEntity>
    {
        IQueryable<TEntity> GetQueryable(Expression<Func<TEntity, bool>> expression);
        IQueryable<TEntity> GetAll();
        Task<TEntity> Add(TEntity entity);
        Task Update(TEntity entity);
        Task Save();
        Task<TEntity> GetById(long id);
    }
}