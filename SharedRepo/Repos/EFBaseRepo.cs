using Microsoft.EntityFrameworkCore;
using SharedRepo.BaseRepo;
using SharedRepo.Context;
using SharedRepo.Context.Base;
using SharedRepo.ReposResultDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace SharedRepo.Repos
{
    public class EFBaseRepo<TEntity> : IEFBaseRepo<TEntity> where TEntity : BaseEntity
    {
        protected ItContext Context;
        protected DbSet<TEntity> Repo;
        public EFBaseRepo(ItContext context)
        {
            Context = context;
            Repo = Context.Set<TEntity>();
        }
        public async Task Update(TEntity entity)
        {
            Repo.Update(entity);
            await Save();
        }
        public IQueryable<TEntity> GetQueryable(Expression<Func<TEntity, bool>> expression)
        {
            return Repo.Where(expression).AsQueryable();
        }
        public IQueryable<TEntity> GetAll()
        {
            return Repo;
        }
        public async Task<TEntity> GetById(long id)
        {
            return await Repo.FindAsync(id);
        }
        public async Task<TEntity> Add(TEntity entity)
        {
            await Repo.AddAsync(entity);
            await Save();
            return entity;
        }

        public async Task Save()
        {
            await Context.SaveChangesAsync();
        }
    }
}