﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SharedRepo.ReposResultDto
{

    /// <summary>
    /// Класс для возвращения результатов действий между слоями Repository/Logic/Controller.
    /// </summary>
    public class OperationResultDto
    {
        /// <summary>
        /// Результат операции (Успешно - Да/Нет).
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public OperationResult Result { get; set; } = OperationResult.Success;

        /// <summary>
        /// Текстовое описание к результату операции (при необходимости).
        /// </summary>
        public string Message { get; set; }
    }

    public enum OperationResult
    {
        Success,
        Failure
    }
}
