﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedRepo.ReposResultDto
{
    public class EntityListResultDto<T> : OperationResultDto
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int PageCount
        {
            get
            {
                var result = 0;

                if (PageSize > 0 && Total > 0)
                {
                    result = (Total / PageSize) + ((Total % PageSize > 0) ? 1 : 0);
                }

                return result;
            }
            private set
            {
            }
        }
        public int Total { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}
