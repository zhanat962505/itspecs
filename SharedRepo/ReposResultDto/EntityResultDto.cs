﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedRepo.ReposResultDto
{
    public class EntityResultDto<T> : OperationResultDto
    {
        public T Item { get; set; }
    }
}
