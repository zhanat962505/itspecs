﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SharedRepo.Context.Base
{
    public class BaseEntity
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [Column("date_create", Order = 2)]
        public DateTime DateCreate { get; set; } = DateTime.Now;
    }
}
