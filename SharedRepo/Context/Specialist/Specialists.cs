﻿using SharedRepo.Context.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SharedRepo.Context.Specialist
{
    /// <summary>
    /// Таблица Айти Специалисты
    /// </summary>
    [Table("it_workers")]
    public class Specialists: BaseEntity
    {
        /// <summary>
        /// ФИО
        /// </summary>
        [Column("fio")]
        public string Fio { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        [Column("birth_date")]
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Город местонахождения
        /// </summary>
        [Column("city")]
        public string City { get; set; }

        /// <summary>
        /// количество лет
        /// </summary>
        [Column("old_year")]
        public int OldYear { get; set; }

        /// <summary>
        /// Образование
        /// </summary>
        [Column("education")]
        public string Education { get; set; }

        /// <summary>
        /// Специализация
        /// </summary>
        [Column("specialization")]
        public string Specialization { get; set; }

        /// <summary>
        /// Профессиональные навыки
        /// </summary>
        [Column("prof_skills")]
        public string ProfSkills { get; set; }

        /// <summary>
        /// контактные данные
        /// </summary>
        [Column("phone_number")]
        public string PhoneNumber { get; set; }
    }
}
