﻿using Microsoft.EntityFrameworkCore;
using SharedRepo.Context.Specialist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedRepo.Context
{
    public class ItContext: DbContext
    {
        public ItContext()
        {
        }

        public ItContext(DbContextOptions<ItContext> options)
            : base(options)
        {

        }
        public DbSet<Specialists> Specialists { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);
        }
    }

}
