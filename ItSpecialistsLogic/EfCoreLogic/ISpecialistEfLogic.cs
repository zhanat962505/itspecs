﻿using ItSpecialistsLogic.Dto;
using SharedRepo.BaseRepo;
using SharedRepo.Context.Specialist;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItSpecialistsLogic.EfCoreLogic
{
    public interface ISpecialistEfLogic: IBaseResultRepository<SpecialistDto>
    {
    }
}
