﻿using ItSpecialistsLogic.Dto;
using Microsoft.EntityFrameworkCore;
using SharedRepo.BaseRepo;
using SharedRepo.Context.Specialist;
using SharedRepo.Repos;
using SharedRepo.ReposResultDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItSpecialistsLogic.EfCoreLogic
{
    public class SpecialistEfLogic : ISpecialistEfLogic
    {
        private readonly IEFBaseRepo<Specialists> _efrepo;
        public SpecialistEfLogic(IEFBaseRepo<Specialists> efrepo)
        {
            _efrepo = efrepo;
        }
        public SpecialistDto MapToDto(Specialists obj) 
        {
            return new SpecialistDto()
            {
                Id = obj.Id,
                Fio = obj.Fio,
                City = obj.City,
                BirthDate = obj.BirthDate,
                Education = obj.Education,
                OldYear = obj.OldYear,
                PhoneNumber = obj.PhoneNumber,
                ProfSkills = obj.ProfSkills,
                Specialization = obj.Specialization
            };
        }
        public Specialists MapToObj(SpecialistDto dto) 
        {
            return new Specialists()
            {
                Id = dto.Id,
                Fio = dto.Fio,
                City = dto.City,
                BirthDate = dto.BirthDate,
                Education = dto.Education,
                OldYear = dto.OldYear,
                PhoneNumber = dto.PhoneNumber,
                ProfSkills = dto.ProfSkills,
                Specialization = dto.Specialization
            };
        }
        public async Task<OperationResultDto> Add(SpecialistDto dto)
        {
            try
            {
                var result = new OperationResultDto();
                var insertedElem = MapToObj(dto);
                var entity = await _efrepo.Add(insertedElem);
                return result;
            }
            catch (Exception ex) 
            {
                return new OperationResultDto { Result = OperationResult.Failure, Message = ex.Message };
            }
        }
        public async Task<OperationResultDto> Update(SpecialistDto dto)
        {
            try
            {
                var result = new OperationResultDto();
                var insertedElem = MapToObj(dto);
                await _efrepo.Update(insertedElem);
                return result;
            }
            catch (Exception ex)
            {
                return new OperationResultDto { Result = OperationResult.Failure, Message = ex.Message };
            }
        }
        public async Task<EntityResultDto<SpecialistDto>> GetById(long id)
        {
            try 
            {
                var result = new EntityResultDto<SpecialistDto>();
                var entity = await _efrepo.GetById(id);
                result.Item = entity!=null? MapToDto(entity): null;
                return result;
            }
            catch (Exception ex) 
            {
                return new EntityResultDto<SpecialistDto> { Result = OperationResult.Failure, Message = ex.Message };
            }
        }

        public async Task<EntityListResultDto<SpecialistDto>> GetItems(SpecialistDto filter)
        {
            try
            {
                var result = new EntityListResultDto<SpecialistDto>
                {
                    PageNumber = (int)filter.Page,
                    PageSize = (int)filter.Limit,
                };

                var resultItems = new List<SpecialistDto>();

                var allElemQuery = _efrepo.GetAll();
                var total =await allElemQuery.CountAsync();
                var elemList = allElemQuery.Skip((filter.Page) * filter.Limit).Take(filter.Limit);
                foreach (var item in elemList) 
                {
                    resultItems.Add(MapToDto(item));
                }

                result.Total = total;
                result.Items = resultItems;
                return result;
            }
            catch (Exception ex)
            {
                return new EntityListResultDto<SpecialistDto> { Result = OperationResult.Failure, Message = ex.Message, Total = 0, PageNumber = filter.Page, PageSize = filter.Limit };
            }
        }
    }
}
