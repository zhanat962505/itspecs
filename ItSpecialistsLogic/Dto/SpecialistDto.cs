﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItSpecialistsLogic.Dto
{
    public class SpecialistDto
    {
        public long Id { get; set; }
        public string Fio { get; set; }
        public DateTime? BirthDate { get; set; }
        public string City { get; set; }
        public int OldYear { get; set; }
        public string Education { get; set; }
        public string Specialization { get; set; }
        public string ProfSkills { get; set; }
        public string PhoneNumber { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; } 
    }
}
