﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ItSpecialistsLogic.EfCoreLogic;
using ItSpecialists.Dto;
using ItSpecialistsLogic.Dto;

namespace ItSpecialists.Controllers
{
    public class SharedController : Controller
    {
        private readonly ISpecialistEfLogic _eflogic;
        public SharedController(ISpecialistEfLogic eflogic)
        {
            _eflogic = eflogic;
        }

        /// <summary>
        /// Регистрация специалиста
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("AddSpecialist")]
        public async Task<IActionResult> AddSpecialist([FromForm] SpecialistsDto dto)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var insertedItem = new SpecialistDto()
                {
                    Fio = dto.Fio,
                    City = dto.City,
                    BirthDate = dto.BirthDate,
                    Education = dto.Education,
                    OldYear = dto.BirthDate!=null? DateTime.Now.Year - dto.BirthDate.Value.Year:0,
                    PhoneNumber = dto.PhoneNumber,
                    ProfSkills = dto.ProfSkills,
                    Specialization = dto.Specialization
                };
                var result = await _eflogic.Add(insertedItem);
                return Json(result);
            }
            catch (Exception ex)
            {
                return BadRequest(new Exception(ex.Message));
            }
        }

        /// <summary>
        /// Обновление данных по айти специалисту
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("UpdateSpecialist")]
        public async Task<IActionResult> UpdateSpecialist([FromForm] SpecialistsDto dto)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var insertedItem = new SpecialistDto()
                {
                    Id = dto.Id!=null?(long)dto.Id: 0,
                    Fio = dto.Fio,
                    City = dto.City,
                    BirthDate = dto.BirthDate,
                    Education = dto.Education,
                    OldYear = dto.BirthDate != null ? DateTime.Now.Year - dto.BirthDate.Value.Year : 0,
                    PhoneNumber = dto.PhoneNumber,
                    ProfSkills = dto.ProfSkills,
                    Specialization = dto.Specialization
                };
                var result = await _eflogic.Update(insertedItem);
                return Json(result);
            }
            catch (Exception ex)
            {
                return BadRequest(new Exception(ex.Message));
            }
        }

        /// <summary>
        /// Возвращает конкретного специалиста по айди
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetSpecialistById")]
        public async Task<IActionResult> GetSpecialistById(long id)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var result = await _eflogic.GetById(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return BadRequest(new Exception(ex.Message));
            }
        }

        /// <summary>
        /// Возвращает всех зарегистрированных специалистов
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet("GetAllSpecialists")]
        public async Task<IActionResult> GetAllSpecialists(int page, int limit)
        {
            try
            {
                var result = await _eflogic.GetItems(new SpecialistDto { Page = page, Limit = limit });
                return Json(result);
            }
            catch (Exception ex)
            {
                return BadRequest(new Exception(ex.Message));
            }
        }




    }
}
