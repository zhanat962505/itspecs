﻿using ItSpecialistsLogic.EfCoreLogic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.OpenApi.Models;
using SharedRepo.Context;
using SharedRepo.Repos;
using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.IO;

namespace ItSpecialists
{
    public class Startup
    {
        private readonly IConfiguration Configuration;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            var timeOut = Configuration["ConnectionStringParams:TimeOut"];
            var connectionStr = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ItContext>(
                options => options.UseSqlServer(connectionStr));
            services.AddScoped<ISpecialistEfLogic, SpecialistEfLogic>();
            services.AddScoped(typeof(IEFBaseRepo<>), typeof(EFBaseRepo<>));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "IT Specialists Api", Version = "v1" });
            });
            services.AddControllers();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            app.UseCors(builder =>
            builder.WithOrigins("http://localhost:4200")
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());
     
            

            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                   Path.Combine(Directory.GetCurrentDirectory(), "StaticFile")),
                RequestPath = "/index",
                EnableDefaultFiles = true
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", "MyAPI V1");
            });
            app.UseDeveloperExceptionPage();
            app.UseRouting();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseDefaultFiles(new DefaultFilesOptions { DefaultFileNames = new List<string> { "index.html" } });
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
