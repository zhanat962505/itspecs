﻿(function () {
    
    //- Start on document function
    $(document).ready(function () {
        //- генерим и выводим на фронт список айти специалистов
        var recordId = 0;
        generateTable();

        //- открыть модальное окно
        $("#myBtn").click(function () {
            // генерим форму
            var modalbodyhtml = document.getElementById("modalbody");
            var specialtsForm = generateSpecialistForm(null);
            modalbodyhtml.innerHTML = specialtsForm;
            $("#myModal").modal();
            document.getElementById("add_button").style.display = 'inline';
            document.getElementById("update_button").style.display = 'none';
        });

        //- при нажатий на запись в таблице открываем модалку и вставляем форму для редактирования спциалиста
        $('#tableWrap').on('click', 'table tr', function () {
            var $this = $(this);
            //получаем айди записи
            var id = $this.attr("id");
            //запрос апи
            $.getJSON(`/GetSpecialistById?id=${id}`, function (specialist) {
                //получаем данные и генерим форму
                var modalbodyhtml = document.getElementById("modalbody");
                var data = specialist.item;
                recordId = data.id;
                var specialtsForm = generateSpecialistForm(data);
                modalbodyhtml.innerHTML = specialtsForm;
                $("#myModal").modal();
                document.getElementById("add_button").style.display = 'none';
                document.getElementById("update_button").style.display = 'inline';
            });
        });

        //- Регистрация нового специалиста
        $("#add_button").click(function () {
            var data =
            {
                id: null,
                fio: $("#fio").val(),
                birthdate: $("#birthdate").val(),
                city: $("#city").val(),
                education: $("#education").val(),
                specialization: $("#specialization").val(),
                profskills: $("#profskills").val(),
                telefon: $("#telefon").val()
            }
            SendToServerForSaveChanges(data, '/AddSpecialist');
        });

        //- Обновление специалиста
        $("#update_button").click(function () {
            var data =
            {
                id: recordId,
                fio: $("#fio").val(),
                birthdate: $("#birthdate").val(),
                city: $("#city").val(),
                education: $("#education").val(),
                specialization: $("#specialization").val(),
                profskills: $("#profskills").val(),
                telefon: $("#telefon").val()
            }
            SendToServerForSaveChanges(data, '/UpdateSpecialist');
        });

    });
    //- on document function End


    //-this functions Start
    //-запрос на обновление и сохранение
    function SendToServerForSaveChanges(data, apiName, recordId) {
        $.post(apiName,
            {
                id: data.id,
                fio: data.fio,
                birthDate: data.birthdate !== "" ? data.birthdate : null,
                city: data.city,
                education: data.education,
                specialization: data.specialization,
                profSkills: data.profskills,
                phoneNumber: data.telefon
            },
            function (data) {
                alert("Успешно внесены данные");
                location.reload();
            });
    }

    //-возвращает форму заполнения данных по айти специалисту
    function generateSpecialistForm(data) {
        var inputVals =
        {
            fio: '',
            birthdate: null,
            oldyear: null,
            city: '',
            education: '',
            specialization: '',
            profskills: '',
            telefon: ''
        }
        if (data != null) {
            inputVals.fio = data.fio;
            inputVals.birthdate = data.birthDate;
            inputVals.oldyear = data.oldYear;
            inputVals.city = data.city;
            inputVals.education = data.education;
            inputVals.specialization = data.specialization;
            inputVals.profskills = data.profSkills;
            inputVals.telefon = data.phoneNumber;
        }
        var inputSpecialistFormHtml = "<form>"
            + "<div class=\"form-group\">"
            + "<label for=\"fio\">ФИО</label>"
            + `<input type=\"text\" class=\"form-control\" id=\"fio\" value="${inputVals.fio}" placeholder=\"Введите ФИО\"/>`
            + "</div>"

            + "<div class=\"form-group\">"
            + "<label for=\"birthdate\">Дата рождения (Надо ввести и дату и время)</label>"
            + `<input type='datetime-local' required class=\"form-control\" id='birthdate' value="${inputVals.birthdate}" placeholder=\"Введите дата рождени\"/>`
            + "<span class=\"validity\"></span>"
            + "</div>"

            + "<div class=\"form-group\">"
            + "<label for=\"oldyear\">Полных лет</label>"
            + `<input type='number' disabled class=\"form-control\" id='oldyear' value="${inputVals.oldyear}" placeholder=\"Полных лет\"/>`
            + "</div>"

            + "<div class=\"form-group\">"
            + "<label for=\"city\">Город местонахождения</label>"
            + `<input type='text' class=\"form-control\" id='city' value="${inputVals.city}" placeholder=\"Город местонахождения\"/>`
            + "</div>"

            + "<div class=\"form-group\">"
            + "<label for=\"education\">Образования</label>"
            + `<input type='text' class=\"form-control\" id='education' value="${inputVals.education}" placeholder=\"Образования\"/>`
            + "</div>"

            + "<div class=\"form-group\">"
            + "<label for=\"specialization\">Специализация</label>"
            + `<input type='text' class=\"form-control\" id='specialization' value="${inputVals.specialization}" placeholder=\"Специализация\"/>`
            + "</div>"

            + "<div class=\"form-group\">"
            + "<label for=\"profskills\">Профессиональные навыки</label>"
            + `<input type='text' class=\"form-control\" id='profskills' value="${inputVals.profskills}" placeholder=\"Профессиональные навыки\"/>`
            + "</div>"

            + "<div class=\"form-group\">"
            + "<label for=\"telefon\">Контактные данные</label>"
            + `<input type='text' class=\"form-control\" id='telefon' value="${inputVals.telefon}" placeholder=\"Контактные данные\"/>`
            + "</div>"
            + "</form>";
        return inputSpecialistFormHtml;
    }

    //-генеририует таблицу список всех зарегистрированных айти специалистов
    function generateTable() {
        $.getJSON('/GetAllSpecialists?page=0&limit=1200', function (allSpecialist) {
            var tableWrap = document.getElementById("tableWrapBd");

            var data = allSpecialist.items;

            var table = "<thead>" +
                "<tr>" +
                "<th scope=\"col\">#</th>" +
                "<th scope =\"col\">ФИО</th><th scope=\"col\">Возраст</th><th scope=\"col\">Профессиональные навыки</th>" +
                "<th scope =\"col\">Образование</th><th scope=\"col\">Контактные данные</th><th scope=\"col\">Специализация</th>" +
                "</tr>" +
                "</thead> <tbody>";
            for (var i = 0; i < data.length; i++) {
                table += `<tr class='tbrows' id='` + data[i].id + "'>";
                table += `<th scope=\"row\">` + i + "</th>";
                table += "<td>" + data[i].fio + "</td>";
                table += "<td>" + data[i].oldYear + "</td>";
                table += "<td>" + data[i].profSkills + "</td>";
                table += "<td>" + data[i].education + "</td>";
                table += "<td>" + data[i].phoneNumber + "</td>";
                table += "<td>" + data[i].specialization + "</td>";
                table += "</tr>";
            }
            table += "</tbody>"
            tableWrap.innerHTML = table;
        });
    }

})();