﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItSpecialists.Dto
{
    public class SpecialistsDto
    {
        public long? Id { get; set; }
        public string Fio { get; set; }
        public DateTime? BirthDate { get; set; }
        public string City { get; set; }
        public string Education { get; set; }
        public string Specialization { get; set; }
        public string ProfSkills { get; set; }
        public string PhoneNumber { get; set; }
    }
}
